#!/bin/bash

min_func(){
	echo -n "minimum," >> log/metrics_agg_"`date +"%Y%m%d%H"`".log
	cat log/metrics_hourly_"`date +"%Y%m%d%H"`".log | awk -F, '
	BEGIN {var_1=15000; var_2=15000; var_3=15000; var_4=15000; var_5=15000; var_6=15000; var_7=15000; var_8=15000; var_9=15000}
	{if($1<var_1) var_1=$1;}
	{if($2<var_2) var_2=$2;}
	{if($3<var_3) var_3=$3;}
	{if($4<var_4) var_4=$4;}
	{if($5<var_5) var_5=$5;}
	{if($6<var_6) var_6=$6;}
	{if($7<var_7) var_7=$7;}
	{if($8<var_8) var_8=$8;}
	{if($9<var_9) var_9=$9;}
	END {print var_1","var_2","var_3","var_4","var_5","var_6","var_7","var_8","var_9}' >> log/metrics_agg_"`date +"%Y%m%d%H"`".log
}

max_func(){
	echo -n "maximum," >> log/metrics_agg_"`date +"%Y%m%d%H"`".log
	cat log/metrics_hourly_"`date +"%Y%m%d%H"`".log  | awk -F, '
	BEGIN {var_1=0; var_2=0; var_3=0; var_4=0; var_5=0; var_6=0; var_7=0; var_8=0; var_9=0}
	{if($1>var_1) var_1=$1;}
	{if($2>var_2) var_2=$2;}
	{if($3>var_3) var_3=$3;}
	{if($4>var_4) var_4=$4;}
	{if($5>var_5) var_5=$5;}
	{if($6>var_6) var_6=$6;}
	{if($7>var_7) var_7=$7;}
	{if($8>var_8) var_8=$8;}
	{if($9>var_9) var_9=$9;}
	END {print var_1","var_2","var_3","var_4","var_5","var_6","var_7","var_8","var_9}' >> log/metrics_agg_"`date +"%Y%m%d%H"`".log
}

avg_func(){
	echo -n "average,"  >> log/metrics_agg_"`date +"%Y%m%d%H"`".log
	cat log/metrics_hourly_"`date +"%Y%m%d%H"`".log  | awk -F, '
	BEGIN {var_1=0; var_2=0; var_3=0; var_4=0; var_5=0; var_6=0; var_7=0; var_8=0; var_9=0}
	{var_1+=$1;}
	{var_2+=$2;}
	{var_3+=$3;}
	{var_4+=$4;}
	{var_5+=$5;}
	{var_6+=$6;}
	{var_7+=$7;}
	{var_8+=$8;}
	{var_9+=$9;}
	END {print var_1/60","var_2/60","var_3/60","var_4/60","var_5/60","var_6/60","var_7/60","var_8/60","var_9/60}' >> log/metrics_agg_"`date +"%Y%m%d%H"`".log
}

#main

echo "type,mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" > log/metrics_agg_"`date +"%Y%m%d%H"`".log

min_func
max_func
avg_func

#crontab untuk file ini: 59 * * * * cd ~/<user_path> && bash aggregate_minutes_to_hourly_log.sh
