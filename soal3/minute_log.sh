#!/bin/bash

free -m /home/user > tmp.txt

echo "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" > text_to_log1.txt
cat tmp.txt | awk '{ORS=""} FNR == 2 {print $2 "," $3 "," $4 "," $5 "," $6 "," $7 ","}' >> text_to_log1.txt
cat tmp.txt | awk '{ORS=""} FNR == 2 {print $2 "," $3 "," $4 "," $5 "," $6 "," $7 ","}' > text_to_log2.txt

cat tmp.txt | awk '{ORS=""}FNR == 3 {print $2 "," $3 "," $4 ","}' >> text_to_log1.txt
cat tmp.txt | awk '{ORS=""}FNR == 3 {print $2 "," $3 "," $4 ","}' >> text_to_log2.txt

echo -n `pwd`"," >> text_to_log1.txt
echo -n `pwd`"," >> text_to_log2.txt

du -sh | awk '{print $1}' >> text_to_log1.txt
du -sh | awk '{print $1}' >> text_to_log2.txt

print_metrics(){
	cat text_to_log1.txt >> log/metrics_"`date +"%Y%m%d%H%M%S"`".log
	cat text_to_log2.txt >> log/metrics_hourly_"`date +"%Y%m%d%H"`".log
}

print_metrics

#crontab untuk file ini: * * * * * cd ~/<user_path> && bash minute_log.sh
