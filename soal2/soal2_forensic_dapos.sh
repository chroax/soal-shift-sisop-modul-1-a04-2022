#!/bin/bash

checkLogExist(){
    if [[ ! -d "$locFolder" ]]; then
        mkdir -p -v $locFolder
    else
    	rm -rv $locFolder
    	mkdir -p -v $locFolder
    fi
}

logAttack(){
    cat $logFile | awk -F: 'NR>1 {gsub(/"/, "", $3) arr[$3]++}
        END {
            for ( i in arr ) {
                avg += arr[i]
            }
            avg = avg/12
            printf "Rata-rata serangan adalah sebanyak %.2f requests per jam\n", avg
        }' >> $locFolder/ratarata.txt
}

logMaxIP(){
    cat $logFile | awk -F: 'NR>1 {gsub(/"/, "", $1) arr[$1]++}
        END {
            for ( ip in arr ){
                if(arr[ip] > max){
                    address = ip
                    max = arr[ip]
                }
            }
            print "IP yang paling banyak mengakses server adalah: " address " sebanyak " max " requests\n"
        }' >> $locFolder/result.txt
}

logCurl(){
    cat $logFile | awk -F: ' /curl/ {counter++}
        END {print "Ada " counter " requests yang menggunakan curl sebagai user-agent\n"}' >> $locFolder/result.txt
}

log2AM(){
    cat $logFile | awk -F: '/2022:02/ {gsub(/"/, "", $1)arr[$1]++}
        END {for ( ip in arr ) {print ip}}' >> $locFolder/result.txt
}

logFile=./log_website_daffainfo.log
locFolder=./forensic_log_website_daffainfo_log

if [[ $(id -u) -ne 0 ]]
then
	    echo "user is not superuser or root"
        echo "[Exit Program]"
	    exit 1
else
    echo "[Program running...]"
    checkLogExist
    logAttack
    logMaxIP
    logCurl
    log2AM
fi
