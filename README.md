# soal-shift-sisop-modul-1-a04-2022 

|     NRP    |     Nama    |
| :--------- |:------------    |
| 5025201098 | Ibra Abdi Ibadihi
| 5025201184 | Cahyadi Surya Nugraha |
| 5025201007 | Sejati Bakti Raga

# Soal shift sisop modul 1 A04 2022

# Soal 1
Output yang diinginkan dari soal 1 adalah untuk membuat sistem register dan login. Dimana setiap kegiatan login atau register akan disimpan pada sebuah log.txt sesuai dengan status dari kegiatan entah itu login. Detail yang disimpan dalam log sendiri adalah tanggal dan waktu kegiatan serta username yang melakukan kegiatan tersebut. Ketika user berhasil login, terdapat 2 command yang nantinya akan dijalankan, yaitu att dan dl. Pilihan att akan menampilkan berapa banyak percobaan yang telah dilakukan user untuk login (baik gagal maupun sukses). Sedangkan, Pilihan dl akan mendownload image dari web yang telah disediakan (https://loremflickr.com/320/240 )sebanyak N kali, kemudian image tersebut akan disimpan di dalam zip.

## A. Buat script register pada register.sh dan sistem login yang dibuat di script main.sh
 
## B. Register 
Setiap register yang sukses akan disimpan pada /users/user.txt. 
User akan menginputkan username dan password. Terdapat beberapa persyaratan untuk pembuatan password, yaitu:
- Minimal 8 karakter
- Memiliki minimal 1 huruf kapital dan 1 huruf kecil
- Alphanumerical
- Tidak boleh sama dengan username

Pada sistem register, yang paling pertama dilakukan adalah memastikan bahwa folder atau file dari output yang akan di redirect nanti ada, maka dari itu dibuatlah fungsi checkFileExist yang berfungsi untuk mengecek apakah folder atau folder output nanti sudah ada atau belum jika belum maka buatkan folder dan file tersebut menggunakan *mkdir -p* (-p ini untuk menghindari error dengan membuat parent direktorinya jika belum ada) dan menggunakan touch untuk membuat filenya.

Tahap selanjutnya adalah mengecek syarat dari password yang telah diberikan yaitu:
- Mengecek minimal 8 karakter dengan mengecek apakah panjang karakter lebih sedikit dari (-lt) 8 atau tidak. Jika iya, maka proses dihentikan dan mengeluarkan message "*Password length should at least be 8 characters*", jika tidak lebih sedikit, maka lanjutkan untuk syarat selanjutnya.
- Mengecek minimal 1 huruf kapital pada password dengan menggunakan grep pada password. Jika tidak ditemukan maka akan mengeluarkan message "*Password must contain at least one uppercase*", jika memenuhi maka melanjutkan ke syarat berikutnya.
- Mengecek minimal 1 huruf kecil pada password dengan menggunakan grep pada password. Jika tidak ditemukan maka akan mengeluarkan message "*Password must contain at least one lowercase*", jika memenuhi maka melanjutkan ke SSproses pencatatan log.

Proses Register selesai.

## C. Pencatatan proses login

Untuk pencatatan proses login, yang harus dilakukan adalah mengecek apakah user yang diinput sudah terdaftar di user.txt dengan menggunakan *grep -q -w* (-w untuk mencocokan seluruh kata). Jika ditemukan maka program akan mengoutputkan "User already exist" dan melakukan pencatatan pada log.txt bahwa kegiatan tidak berhasil ($dates $time REGISTER:ERROR User already exist) dan program akan terhenti.

Jika tidak ditemukan maka sistem register akan melakukan validasi password dengan user akan diminta untuk memasukkan passwordnya sebanyak dua kali yang akan digunakan untuk memvalidasi dan memastikan kembali terhadap user bahwa user tersebut akan menggunakan password yang dituliskan pada field password sebelumnya. Pengecekan kesamaan password dan confirmpassword menggunakan operasi aritmatika !=, dimana jika hasilnya true atau artinya tidak sama maka akan mengoutputkan ke terminal "*Password is not matches*" dan melakukan exit. Jika bernilai false maka program akan lanjut ke tahap selanjutnya.

Ketika semua persyaratan password dan username terpenuhi, maka print pesan log di log.txt dengan format MM/DD/YY HH:MM:SS  REGISTER:
INFO User *USERNAME* registered successfully. Dan disimpan pada user.txt dengan format MM/DD/YY HH:MM:SS *Username* *Password*

## D. Memasukkan command setelah login
Pada sistem login (main.sh). User akan memasukkan username serta password yang telah dia registerkan pada user.txt atau yang telah tercatat pada user.txt
Jika user salah dalam memasukkan username ataupun password yang telah ada maka akan membuat sebuah log failed pada log.txt atas nama username tersebut. Sedangkan
jika user berhasil login user akan melakukan pencatatan juga pada log.txt bahwa login berhasil dengan username tersebut. Untuk melakukan validasi cukup melakukan
syntax grep - w "$username $password" yang artinya akan mengambil sebuah kata sesuai isi dari variabel username dan password yang berada pada dalam satu baris yang sama dengan dipisahkan oleh sebuah spasi.


Pada soal jika user berhasil melakukan login maka akan terjadi dua pilihan yaitu melakukan command att yang artinya adalah menghitung percobaan login baik itu berhasil atau gagal yang telah dilakukan oleh username tersebut.
Hal ini dapat dilakukan dengan melihat argumen ke 5 (jika login berhasil) atau argumen ke 9 (jika login gagal) pada log.txt. nantinya setiap perjumpaan argumen ke 5 atau 9 yang sama valuenya dengan variabel $username maka sebuah counter akan ditambahkan satu.
Lalu pada akhir nanti banyaknya counter tersebut akan dikurangi 1 karena 1nya sudah pasti merupakan log sebuah registrasi dengan username tersebut. (Jika sebuah login berhasil maka sudah pasti username tersebut telah melalui registrasi dan setiap registrasi akan dicatat pada log.txt)

Lalu untuk command yang satunya adalah command dl N dimana user akan diminta untuk mendownload sebuah foto dari website yang telah diberikan sebanyak N (argumen yang dimasukkan user). Kemudian foto tersebut akan dizip dan password sesuai dengan penamaan yang telah ada di soal. Apabila terdapat nama zip yang sama,
maka zip akan dibuka, kemudian foto baru akan didownload dan akan digabungkan ke dalam zip yang lama. Oleh sebab itu, kita perlu mengecek apakah sudah ada file zip tersebut.
Untuk melakukannya akan dilakukan dengan melakukan -f $fileName.zip dan jika sudah ada maka file tersebut akan di unzip terlebih dahulu jika belum ada akan dilakukan pembuatan folder dengan nama tersebut dan dilakukan fungsi pendowloadan foto yang diakhiri dengan zip pada folder yang telah dibuat.



## Output
1. Ini terjadi ketika password dan username sama. Akibatnya, tidak akan terjadi pencatatan baik di log maupun user.txt. Di sini dibuat 2 folder baru karena folder user dan log belum ada.

![satu](img/messageImage_1646052836901.jpeg)

2. Di sini terlihat pembuatan akun berhasil karena password memenuhi syarat dari soal dan terjadi pencatatan pada log.txt dan user.txt.

![dua](img/messageImage_1646053006920.jpg)

3. Output sebagai berikut akan ditampilkan ketika user mencoba login, tetapi memasukkan username atau password yang salah (tidak terdaftar pada user.txt)

![tiga](img/messageImage_1646053096238.jpg)

4. Hasil dari percobaan login yang gagal akan dicatat pada log.txt 

![empat](img/4.jpg)

5. Ketika user berhasil login, maka akan terdapat dua pilihan command, yaitu att dan dl. Command yang dijalankan sesuai dengan perintah yang terdapat dalam soal. 

![lima](img/5.jpg)

6. Kita dapat melihat bahwa setelah menjalankan perintah dl, akan muncul file zip dengan format nama yang sesuai dengan ketentuan pada soal.

![enam](img/6.jpg)

## Kendala
- Mencari cara yang tepat untuk pengecekan password.

- Mencari cara yang tepat untuk mengecek kesamaan password serta username yang telah terdaftar.

- Mencari cara untuk melakukan unzip dan rezip kembali dengan gambar yang telah didowonload (Lihat soal 1 bagian D yang i)

# Soal 2
Output yang diinginkan dari soal2 adalah untuk membuat suatu program yang dapat melakukan analisis melalui scripting awk. Dengan kasus pada soal berupa website (https://daffa.info) yang dihack oleh seseorang, user diminta untuk menganalisis rerata request per jam yang dikirimkan penyerang ke website, mengidentifikasi IP yang paling banyak melakukan request ke server, jumlah request dengan user-agent curl, serta daftar IP yang mengakses website pada jam 2 pagi. Nantinya output tersebut akan disimpan pada *ratarata.txt* dan *result.txt*.

## A. Membuat folder log
Sebuah file log akan dibutuhkan untuk mencatat proses selama program berjalan di sesi tertentu. Jika file dalam direktori yang diberikan (dalam kasus ini: ~/logfile_daffainfo.log) belum tersedia, maka file log harus dibuat terlebih dahulu. Sebaliknya, jika file sudah tersedia, maka sesi log tersebut akan dihapus dan diganti dengan sesi baru, sehingga proses yang dilakukan adalah menghapus file log sebelumnya dan membuat file log yang baru. Dilakukan juga validasi folder yang memiliki tujuan yang sama dengan validasi log file. Metode yang digunakan adalah dengan shell script: *-d "<lokasi folder>"* untuk mendeteksi ada atau tidaknya file pada lokasi tersebut. 

## B. Rata-Rata Serangan (x Request per Jam)
Setelah melakukan validasi folder dan file, hal yang selanjutnya dilakukan adalah mengoutputkan hal-hal yang telah diminta pada soal. Pada kasus ini, hal pertama yang diminta dioutputkan dari soal adalah rata-rata serangan yang akan disimpan pada file *ratarata.txt* dengan format penulisan yang telah dideskripsikan pada soal.
Untuk melakukan penghitungan rerata serangan, digunakanlah AWK looping melalui array yang telah diolah melalui *gsub* (global substitution) untuk mensubstitusikan input IP yang ada menjadi format IP yang diinginkan. Setelah itu dilakukan, program akan melakukan loop dalam array yang telah diolah dengan variabel "i" yang menotasikan setiap nilai dalam array tersebut. Dalam loop tersebut, akan dilakukan increment pada variabel *hour* dan addition pada nilai rerata (*avg) sebanyak nilai array pada index ke-i. Setelah loop tersebut selesai, baru dilakukan averaging dengan membagi variabel **avg* dengan jumlah jam (*hour) yang telah diproses dari loop. Kemudian hasil berupa nilai dari **avg* akan diprint dan dimasukkan ke dalam file *ratarata.txt*.

## C. IP yang Paling Banyak Mengakses Server
Lalu output berikutnya adalah mencari tahu IP yang paling banyak mengakses server yang akan disimpan pada file result.txt dengan format penulisan yang telah dideskripsikan pada soal.
Untuk mengetahui IP yang paling banyak mengakses server, dilakukan langkah yang mirip seperti sebelumnya hanya saja kondisi pada *gsub* sedikit diubah menyesesuaikan dengan kasus yang diinginkan. Kemudian cari IP dan jumlah request terbanyak yang ada dalam array, Variabel "*address" merupakan IP yang paling banyak mengakses server dan variabel "max*" adalah jumlah request terbanyak.

## D. Requests yang menggunakan user-agent curl
Untuk output ke-3 sama seperti sebelumnya akan dioutputkan ke dalam file *result.txt. Untuk menghitung banyak request yang menggunakan curl sebagai user-agent, digunakan AWK dimana setiap ditemukan curl sebagai user agent maka variabel n terus bertambah. Hasil n merupakan banyaknya request yang menggunakan curl sebagai user agent akan diprint dan dimasukkan ke dalam file **result.txt*

## E. IP yang mengakses website pada jam 2 pagi
Output terakhir juga akan disimpan ke dalam file *result.txt. Untuk mencari IP yang mengakses website pada jam 2 pagi, digunakan AWK dimana setiap ditemukan 2022:02 maka **arr[i]* terus bertambah. Kemudian menggunakan looping akan di print IP yang mengakses pada jam 2 pagi.

## Output
1. Program yang kita buat membutuhkan akses dari superuser atau root sehingga kita harus masuk terlebih dahulu dengan sudo su. Setelah kita masuk dan menjalankan program, dapat dilihat bahwa terdapat sebuah folder baru sesuai ketentuan format soal.

![tujuh](img/7.jpg)

2. Program di atas akan menghasilkan dua output file yaitu ratarata.txt serta result.txt.

![delapan](img/8.jpg)

3. Isi file result.txt

![sembilan](img/9.jpg)

4. Isi file ratarata.txt

![sepuluh](img/10.jpg)


## Kendala
- Menemukan syntax, option, serta cara looping yang tepat pada penggunaan AWK sehingga memenuhi kondisi-kondisi yang diberikan oleh soal

- Pada awal sebelum menjalankan program terlihat hanya 2 file saja yaitu log website serta program yang telah kita buat.

- Program yang kita buat membutuhkan akses dari superuser atau root sehingga kita harus masuk terlebih dahulu dengan *sudo su*. Setelah kita masuk dan menjalankan program, dapat dilihat bahwa terdapat sebuah folder baru sesuai ketentuan format soal.

# Soal 3
Output yang diinginkan soal3 berupa file log yang berisi metrics untuk memonitoring resources yang tersedia pada computer. Resources yang perlu dimonitoring adalah ram dan size suatu directory. Untuk ram gunakan command *free -m. Untuk disk gunakan command **du -sh* . Catat semua metrics yang didapatkan dari hasil *free -m. Untuk hasil **du -sh, catat size dari path directory tersebut. Untuk target_path yang akan dimonitor adalah */home/{user}/**. 

Monitoring dilakukan setiap menit dan setiap jam akan ditampilkan metrics minimum, maximum, dan rata-ratanya.

## A. Metrics log

File *minute_log.sh* akan mencatat *mem_total, **mem_used, **mem_free, **mem_shared, **mem_buff, **mem_available, **swap_total, **swap_used, **swap_free, **path, **path_size* setiap dijalankan pada sebuah file txt yang akan diteruskan ke sebuah file .log dengan format penamaan *metrics_{YmdHms}*.log. Pada tahap ini pengecekan memory yang kosong menggunakan command *free -m,  path directory menggunakan **echo -n ‘pwd’”,”,* dan size menggunakan *du -sh.*

## B. Pencatatan per menit

Untuk menjalankan script secara otomatis menggunakan crontab. Script *minute_log.sh* dijalankan tiap menit dan *aggregate_minutes_to_hourly_log.sh* dijalankan tiap jam.

## C. Script agregasi

- Redirect metrics ke file text_to_log.txt

Metrics memory yang sudah didapatkan dari Langkah sebelumnya diredirect ke file *text_to_log.txt* menggunakan AWK untuk setiap elemen yang didapatkan.
Selanjutnya untuk path menggunakan sintaks *echo -n ‘pwd’”,”* dan size directory menggunakan *du -sh* kemudian diredirect ke *text_to_log.txt.*

- Redirect file txt_to_log.txt ke file log metrics

Data metrics pada *text_to_log.txt* 1 dan 2 selanjutnya diredirect masing-masing ke *metrics_{YmdHms}.log* dan *metrics_hourly_{YmdH}.log* menggunakan fungsi *print_metrics()* yang sudah dibuat.

- aggregate_minutes_to_hourly_log.sh

File ini akan mencatat metrics minimum, maximum, dan rata-rata setiap jam menggunakan 3 fungsi dengan kegunaan masing-masing. 3 fungsi tersebut adalah *min_func(), **max_func(), dan **avg_func(). Fungsi ini memiliki cara kerja yang sangat mirip yakni menggunakan awk untuk mengakses data metrics pada file **metrics_hourly_{YmdH}.log. menentukan nilai minimum, maximum, dan rata-ratanya dengan membandingkan data metrics pada jam itu. Selanjutnya hasil perbandingan akan diredirect ke file **metrics_agg_{YmdH}.log*.

## Output

1. Contoh file metrics_{YmdHms}.log:

![sebelas](img/11.png)
 
2. Contoh file metrics_hourly_{YmdH}.log setelah beberapa kali dijalankan:
 
![duabelas](img/12.png)

3. Contoh file metrics_agg_{YmdH}.log pada suatu jam:

![tigabelas](img/13.png)

## Kendala

- Mencoba menjalankan menggunakan crontab 

- Redirect file metrics menggunakan AWK

- Operasi perbandingan menggunakan AWK
