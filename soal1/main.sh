#!/bin/bash

userLogin(){
    if grep -q -w "$username $pass" "$txtUser" ; then
        echo $dates $time LOGIN:INFO User $username logged in >> $txtLog
        echo "Successfully logged in"
        read -p "Choose command (dl | att): " command
        if [ $command == att ]; then
            att
        elif [ $command == dl ]; then
            read -p "Quantity: " number
            dl
        else
            echo "$command: command not found"
        fi
    else
        echo $dates $time LOGIN:ERROR Failed login attemp on user $username >> $txtLog
        echo -e "Failed login\nWrong username or password"
    fi
}
att(){
	awk -v user="$username" 'BEGIN {N=0} $5 == user || $9 == user {N++} END {print N-1}' $txtLog 
}

dl(){
    if [[ ! -f "$fileName.zip" ]]; then
        mkdir -p $fileName
        getPhoto
    else
        unzipFile
    fi
}

unzipFile(){
    unzip -P $pass $fileName.zip
    rm -v $fileName.zip
    count=$(find $fileName -type f | wc -l)
    getPhoto
}

getPhoto(){
    for(( i=$count+1; i<=$number+$count; i++ )); do
		wget https://loremflickr.com/320/240 -O $fileName/PIC_$i.jpg
	done

	zip --password $pass -r $fileName.zip $fileName
	rm -rv $fileName 
}

echo "---Login---"
read -p "Enter Username: " username
read -s -p "Enter Password: " pass
echo ""

txtUser=./users/user.txt
txtLog=./soal-shift-sisop-modul-1-a04-2022/log.txt
count=0

dates=$(date +%D)
time=$(date +%T)

fileName=$(date +%Y-%m-%d)_$username

userLogin
